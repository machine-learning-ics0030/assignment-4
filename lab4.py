#!/usr/bin/env python3
import os
import warnings

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans
from sklearn.manifold import TSNE

from common import describe_data, test_env


def plot_clusters(X, y, centers, title, x_label, y_label, file=''):
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap='rainbow')
    plt.scatter(centers[:, 0], centers[:, 1], color='black')
    plt.title(title)
    plt.xlabel(x_label)
    plt.ylabel(y_label)

    if file:
        plt.savefig(file)

    plt.show()


def plot_clusters_tsne(X, y, figure, file=''):
    colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
              'tab:brown', 'tab:pink', 'tab:olive', 'tab:grey', 'aqua',
              'indigo', 'gold', 'peru', 'greenyellow', 'xkcd:salmon']
    markers = ['x', 'X', 's', 'D', '*',
               'H', '1', 'o', 'P', '_',
               '2', '3', '$...$', '8', 'p']
    color_idx = 0
    marker_idx = 0

    plt.figure(figure)

    for cluster in range(0, len(set(y))):
        plt.scatter(X[y == cluster, 0], X[y == cluster, 1],
                    s=5, c=colors[color_idx], marker=markers[marker_idx])
        color_idx = 0 if color_idx == (len(colors) - 1) else color_idx + 1
        marker_idx = 0 if marker_idx == (len(markers) - 1) else marker_idx + 1

    plt.title(figure)
    plt.xticks([])
    plt.yticks([])

    if file:
        plt.savefig(file)

    plt.show()


def songs_clustering(features, k_range=0, n_clusters=0, title='', figure_num=None):
    X = songs_df.loc[:, features].values
    scaler_X = StandardScaler()
    scaler_X.fit(X)
    X_scaled = scaler_X.transform(X)
    dir_path = 'results/2D_clusters_%s/' % str(figure_num)
    if (k_range):
        wcss = []
        for k in range(1, k_range):
            kmeans = KMeans(n_clusters=k, init='k-means++')
            kmeans.fit(X_scaled)
            wcss.append(kmeans.inertia_)
        plt.figure()
        plt.grid()
        plt.plot(range(1, k_range), wcss, linewidth=2,
                 color='blue', marker='s')
        plt.xlabel('K Value X (%s)' % ','.join(features))
        plt.ylabel('WCSS')
        if (not os.path.exists(dir_path)):
            os.mkdir(dir_path)
        plt.savefig(dir_path + 'wcss')
        plt.show()

    if (n_clusters):
        kmeans = KMeans(n_clusters=n_clusters)
        y_kmeans = kmeans.fit_predict(X_scaled)
        centers = scaler_X.inverse_transform(kmeans.cluster_centers_)
        if (len(features) == 2):
            plot_title = title if title else 'Songs Clustering by %s and %s' % (
                features[0], features[1])
            if (not os.path.exists(dir_path)):
                os.mkdir(dir_path)
            clusters_file = dir_path + \
                str(n_clusters) + '_clusters' if figure_num is not None else ''
            plot_clusters(X, y_kmeans, centers, plot_title,
                          features[0], features[1], clusters_file)
        else:
            raise ValueError(
                'For more than 2 dimensions, choose songs_clustering_tsne')


def songs_clustering_tsne(features, k_range=0, n_clusters=0, title='', figure_num=None):
    X = songs_df.loc[:, features].values
    scaler_X = StandardScaler()
    scaler_X.fit(X)
    X_scaled = scaler_X.transform(X)
    dir_path = 'results/tsne_%s (%d features)/' % (str(figure_num),
                                                   len(features))
    if len(features) == 2:
        warnings.warn(
            'For 2 dimensions, it is better to use songs_clustering() function')

    if k_range:
        wcss = []
        for k in range(1, k_range):
            kmeans = KMeans(n_clusters=k, init='k-means++')
            kmeans.fit(X_scaled)
            wcss.append(kmeans.inertia_)
        plt.figure()
        plt.grid()
        plt.plot(range(1, k_range), wcss, linewidth=2,
                 color='blue', marker='s')
        plt.xlabel('K Value X \n (%s)' % ','.join(features))
        plt.ylabel('WCSS')
        if (not os.path.exists(dir_path)):
            os.mkdir(dir_path)
        plt.savefig(dir_path + 'wcss')
        plt.show()

    if (n_clusters):
        kmeans = KMeans(n_clusters=n_clusters)
        y_kmeans = kmeans.fit_predict(X_scaled)
        X_tsne = TSNE(n_components=2,
                      random_state=0).fit_transform(X_scaled)

        if (not os.path.exists(dir_path)):
            os.mkdir(dir_path)
        no_clusters_file = dir_path + '0_clusters' if figure_num is not None else ''
        clusters_file = dir_path + \
            str(n_clusters) + '_clusters' if figure_num is not None else ''
        plot_clusters_tsne(X_tsne,
                           np.full(X_tsne.shape[0], 0),
                           title if title else 't-SNE visualisation without clusters \n (%s)' % ', '.join(
                               features),
                           no_clusters_file
                           )
        plot_clusters_tsne(X_tsne, y_kmeans,
                           title if title else 'T-SNE visualisation with clusters \n (%s)' % ', '.join(
                               features),
                           clusters_file
                           )


if __name__ == '__main__':
    modules = ['numpy', 'pandas', 'sklearn', 'matplotlib', 'seaborn']
    test_env.versions(modules)
    songs_df = pd.read_csv('data/spotify_dataset.csv', sep=',')

# Initial Dataset Description
describe_data.print_overview(
    songs_df, file='results/initial_songs_overview.txt')
describe_data.print_categorical(
    songs_df, file='results/initial_songs_categorical.txt')

# Dataset Preprocessing

# drop unimportant features for music selection
songs_df.drop(['Week of Highest Charting', 'Artist', 'Song Name', 'Number of Times Charted',
               'Song ID', 'Genre', 'Chord', 'Index', 'Weeks Charted', 'Release Date'], inplace=True, axis=1)

# drop 'Streams' and 'Artist Followers' due to invalid format for str-to-num conversion
songs_df.drop(['Streams', 'Artist Followers'], axis=1, inplace=True)

# convert numerical data written in object dtype to float
songs_df['Danceability'] = pd.to_numeric(
    songs_df['Danceability'], errors='coerce')
songs_df['Energy'] = pd.to_numeric(songs_df['Energy'], errors='coerce')
songs_df['Speechiness'] = pd.to_numeric(
    songs_df['Speechiness'], errors='coerce')
songs_df['Loudness'] = pd.to_numeric(songs_df['Loudness'], errors='coerce')
songs_df['Acousticness'] = pd.to_numeric(
    songs_df['Acousticness'], errors='coerce')
songs_df['Liveness'] = pd.to_numeric(songs_df['Liveness'], errors='coerce')
songs_df['Tempo'] = pd.to_numeric(songs_df['Tempo'], errors='coerce')
songs_df['Duration (ms)'] = pd.to_numeric(
    songs_df['Duration (ms)'], errors='coerce')
songs_df['Valence'] = pd.to_numeric(songs_df['Valence'], errors='coerce')
songs_df['Popularity'] = pd.to_numeric(songs_df['Popularity'], errors='coerce')

# Drop 11 rows with NaN values (Highest Ranking Positions - NaN)
songs_df.dropna(axis=0, inplace=True)

# Final Dataset Description
describe_data.print_overview(
    songs_df, file='results/resulting_songs_overview.txt')
describe_data.print_categorical(
    songs_df, file='results/resulting_songs_categorical.txt')

# 2-DIMENSIONAL CLUSTERING (NO TSNE)

# Danceability & Popularity Clustering
# get elbow plot to find n_clusters
songs_clustering(
    features=['Danceability', 'Popularity'], k_range=15, figure_num=0)
# Result: best n_clusters = 5
songs_clustering(features=['Danceability', 'Popularity'],
                 n_clusters=5, figure_num=0)

# Tempo, Valence Clustering
songs_clustering(features=['Tempo', 'Valence'], k_range=15, figure_num=1)
# Result: best n_clusters = 4
songs_clustering(features=['Tempo', 'Valence'], n_clusters=6, figure_num=1)

# Tempo, Energy Clustering
songs_clustering(features=['Tempo', 'Energy'], k_range=15, figure_num=2)
# Result: best n_clusters = 6
songs_clustering(features=['Tempo', 'Energy'], n_clusters=6, figure_num=2)

# Loudness, Energy Clustering
songs_clustering(features=['Loudness', 'Energy'], k_range=15, figure_num=3)
# Result: best n_clusters = 6
songs_clustering(features=['Loudness', 'Energy'], n_clusters=6, figure_num=3)

# MULTIDIMENSIONAL CLUSTERING (WITH TSNE)


# Valence, Energy, Danceability, Tempo Clustering
songs_clustering_tsne(
    features=['Valence', 'Energy', 'Danceability', 'Tempo'], k_range=25, figure_num=0)
# Result: best n_clusters = 10
songs_clustering_tsne(features=['Valence', 'Energy', 'Danceability', 'Tempo'],
                      n_clusters=10,
                      figure_num=0)

# Acousticness, Danceability, Tempo Clustering
songs_clustering_tsne(
    features=['Acousticness', 'Danceability', 'Tempo'], k_range=25, figure_num=1)
# Result: best n_clusters = 10
songs_clustering_tsne(
    features=['Acousticness', 'Danceability', 'Tempo'], n_clusters=10, figure_num=1)

# Loudness, Popularity, Duration, Tempo, Acousticness, Valence
songs_clustering_tsne(features=['Loudness', 'Popularity', 'Duration (ms)', 'Tempo', 'Acousticness', 'Valence'],
                      k_range=25,
                      figure_num=2)
# Result: best n_clusters = 10
songs_clustering_tsne(features=['Loudness', 'Popularity', 'Duration (ms)', 'Tempo', 'Acousticness', 'Valence'],
                      n_clusters=10,
                      figure_num=2)

# All 11 Features
songs_clustering_tsne(features=['Highest Charting Position', 'Popularity', 'Danceability',
                                'Energy', 'Loudness', 'Speechiness',
                                'Acousticness', 'Liveness', 'Tempo',
                                'Duration (ms)', 'Valence'
                                ], k_range=25, figure_num=3)
# Result: best n_clusters = 15
songs_clustering_tsne(features=['Highest Charting Position', 'Popularity', 'Danceability',
                                'Energy', 'Loudness', 'Speechiness',
                                'Acousticness', 'Liveness', 'Tempo',
                                'Duration (ms)', 'Valence'
                                ],
                      n_clusters=15,
                      title='T-SNE visualisation with clusters \n (All 11 Features)',
                      figure_num=3)

<h2>Lab Overview</h2>

<h3>Problem</h3>

In this lab, I used K-means and TSNE methods to cluster songs from the <a href="https://www.kaggle.com/sashankpillai/spotify-top-200-charts-20202021" target="_blank">“Spotify Top 200 Charts (2020-2021)” dataset</a> and to visualize the results.

The ML solutions to the problem of clustering songs by their different characteristics can easily find their application in such businesses as streaming music or stock music services, where the user can be advised by ML algorithm with a song that is closely located in the same cluster to the song that was already previously chosen by a person when searching for the specific type of music (which is probably already done by companies like Spotify or Shutterstock).

Therefore, the problem that I aimed to solve in this lab was to cluster songs by their different features, so they can be advised to music services' users when the latter ones search for a song using specific filters. 

<h3>Approach and Solution</h3>

The whole job of clustering is done by *songs\_clustering* (for 2-D data) and *songs\_clustering\_tsne* (for multidimensional data) functions. These functions, in turn, call *plot\_clusters* or *plot\_clusters\_tsne* to visualize the results. Before the clustering itself, they extract the requested features from the *songs\_df* dataset and scale them, as the K-means is a distance-based algorithm, while the features vary widely from 100s and 1000s values (like Tempo or Duration) to 0.1s values (like Valence or Danceability). In 2-features clusters, the plot also includes x and y ticks which represent the real values of features and cluster centers (which were inversely transformed with a scaler).

Additionally, before the clustering functions are called at all, some dataset preprocessing is also done, such as dropping unimportant features and transforming numbers in *object* format to *float* format. 

Finally, the plots are stored in the results directory each in its own folder along with the WCSS plot which I used to find the best number of clusters for a particular set of features.


<h3>Results Selection</h3>

In the _results_ folder, I created several clustering examples with different features, as while searching for music, people tend to use different characteristics to filter the songs (e.g., “music for slow dance” or “popular acoustic energetic songs”, or "slow and sad songs”). Spotify has already done its job of identifying the main songs’ features, which include such characteristics as _Loudness_, _Valence_ (defines the mood of music from negative to positive), _Acousticness_, _Danceability_, _Tempo_, _Duration_, _Energy_, _Popularity_, etc. The main thing to do was to cluster songs by combining different features and find the clusters in the specific categories (like "Popular Songs for dancing"), projecting them on the plots in the _results_ directory.

<h3>Evaluation and Summary</h3>

To sum up, the K-means clustering performed its job quite good and I can say that the goal of songs' clustering by different features was achieved. However, the execution time of the algorithm also greatly rose after more dimensions were added (the last example with 11 features and 15 clusters took considerably more time to give the result than the previous ones). Additionally, one more disadvantage of K-means was also that for each feature set the appropriate number of clusters should have been manually chosen by generating the WCSS plot and analyzing that.
